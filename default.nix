with (import <nixpkgs> {});
stdenv.mkDerivation rec {
   name = "maze-ca";
   buildInputs = [
     gcc
     gnumake
     SDL2
   ];
}
